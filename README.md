# Overlay
Overlay File System

Overlay is an easy way to implement a file system in such a way that
production assets can be overridden by assets on disk.

## Usage

```go
package main

import (
	"embed"
	
	"go.jolheiser.com/overlay"
)

//go:embed assets
var assets embed.FS

func main() {
	ofs, err := overlay.New("/var/lib/myapp/custom", assets)
	if err != nil {
		panic(err)
    }
    ...
}
```

If `/var/lib/myapp/custom` has an `assets` sub-directory, this implementation works.

However, if `/var/lib/myapp/custom` matches the `assets` directory layout, 
you can use `WithSub` like so...

```go
package main

import (
	"embed"
	
	"go.jolheiser.com/overlay"
)

//go:embed assets
var assets embed.FS

func main() {
	ofs, err := overlay.New("/var/lib/myapp/custom", assets, overlay.WithSub("assets"))
	if err != nil {
		panic(err)
    }
    ...
}
```

## License

[MIT](LICENSE)