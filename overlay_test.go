package overlay

import (
	"embed"
	"io"
	"io/fs"
	"os"
	"strings"
	"testing"
)

//go:embed _test/embed
var embedded embed.FS

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestOverlay(t *testing.T) {
	tt := []struct {
		Name     string
		File     string
		Expected string
	}{
		{
			Name:     "Embed",
			File:     "test1.txt",
			Expected: "test1",
		},
		{
			Name:     "Disk",
			File:     "test2.txt",
			Expected: "test3",
		},
	}

	x, err := New("_test/disk", embedded, WithSub("_test/embed"), WithCaching(false))
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			fi, err := x.Open(tc.File)
			if err != nil {
				t.Log(err)
				t.FailNow()
			}
			defer fi.Close()

			contents, err := io.ReadAll(fi)
			if err != nil {
				t.Log(err)
				t.FailNow()
			}

			if !strings.EqualFold(string(contents), tc.Expected) {
				t.Logf("fs did not match:\n\tgot: %s\n\texpected: %s\n", string(contents), tc.Expected)
				t.FailNow()
			}
		})
	}
}

func TestWalk(t *testing.T) {
	expected := []string{"test1.txt", "test2.txt"}

	x, err := New("_test/disk", embedded, WithSub("_test/embed"), WithCaching(false))
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	found := make([]string, 0)
	if err := fs.WalkDir(x, ".", func(walkPath string, walkEntry fs.DirEntry, walkErr error) error {
		if walkErr != nil {
			return walkErr
		}

		if walkEntry.IsDir() {
			return nil
		}

		found = append(found, walkEntry.Name())

		return nil
	}); err != nil {
		t.Log(err)
		t.FailNow()
	}

	for _, e := range expected {
		exists := false
		for _, f := range found {
			if e == f {
				exists = true
				break
			}
		}
		if !exists {
			t.Logf("could not find %s in %s\n", e, found)
			t.Fail()
		}
	}
}

var emptyFS embed.FS

func TestInvalid(t *testing.T) {
	_, err := New("/var/lib/myapp/assets/custom", emptyFS)
	if err != nil {
		t.Log("invalid FS should not error explicitly")
		t.FailNow()
	}
}

func BenchmarkCache(b *testing.B) {
	x, err := New("_test/disk", embedded)
	if err != nil {
		b.Log(err)
		b.FailNow()
	}

	for idx := 0; idx < b.N; idx++ {
		x.exists("test2.txt")
	}
}

func BenchmarkNoCache(b *testing.B) {
	x, err := New("_test/disk", embedded, WithCaching(false))
	if err != nil {
		b.Log(err)
		b.FailNow()
	}

	for idx := 0; idx < b.N; idx++ {
		x.exists("test2.txt")
	}
}
